/** 
 * @file llsdserialize_json.cpp
 * @brief JSON parsers and formatters for LLSD
 *
 * $LicenseInfo:firstyear=2006&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

/**
 * This may be lazy or something, but it's the easiest way, so
 * leave me alone.  * Not too sure why LL don't use Boost to phrase
 * XML, then again, that might be  * related to the patch I had to
 * apply, to boost to get this to actually work:
 *
 * https://svn.boost.org/trac/boost/ticket/5281
 * https://svn.boost.org/trac/boost/changeset/72003
 */

#include "linden_common.h"
#include "llsdserialize_json.h"
#include "llsdserialize.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

/**
 * LLSDJSONFormatter
 */
LLSDJSONFormatter::LLSDJSONFormatter()
{
}

// virtual
LLSDJSONFormatter::~LLSDJSONFormatter()
{
}

// virtual
S32 LLSDJSONFormatter::format(const LLSD& data, std::ostream& ostr, U32 options) const
{
	// Convert LLSD to XML format.
	std::stringstream xml;
	LLPointer<LLSDXMLFormatter> f = new LLSDXMLFormatter;
	S32 pass = f->format(data, xml, LLSDFormatter::OPTIONS_NONE);
	if(pass == 0) return 0;

	// Read the XML and convert it to JSON.
	boost::property_tree::ptree pt;
	try{ boost::property_tree::xml_parser::read_xml(xml, pt); }catch(...){ return 0; }
	try{ boost::property_tree::json_parser::write_json(ostr, pt, options); }catch(boost::bad_any_cast &){ return 0; }
	return pt.size();
}

/**
 * LLSDJSONParser
 */
LLSDJSONParser::LLSDJSONParser()
{
}

LLSDJSONParser::~LLSDJSONParser()
{
}

// virtual
S32 LLSDJSONParser::doParse(std::istream& input, LLSD& data) const
{
	// Read the JSON file and convert it to XML.
	boost::property_tree::ptree pt;
	std::stringstream xml;
	try{ boost::property_tree::json_parser::read_json(input, pt); }catch(...){ return 0; }
	try{ boost::property_tree::xml_parser::write_xml(xml, pt); }catch(boost::bad_any_cast &){ return 0; }

	// Read the XML and convert it into LLSD.
	return LLSDSerialize::fromXML(data, xml);
}
