/** 
 * @file exobridge.cpp
 *
 * eAPI is a simple set of tools designed to provide content
 * creators with the opportunity to use optional viewer side functions
 * in Exodus for their products in Second Life.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "exobridge.h"

// Stuff
#include "llagent.h"
#include "llstartup.h"
#include "llviewercontrol.h"
#include "llviewermessage.h"
#include "llviewerobject.h"
#include "llviewerobjectlist.h"
#include "llviewerwindow.h"

#include "llviewerstats.h"
#include "llnotificationmanager.h"
#include "llappearancemgr.h"

//math
#include "v3math.h"

// Raycasting
#include "pipeline.h"
#include "raytrace.h"

// Windlight
#include "rlvextensions.h"
#include "llwlparammanager.h"
#include "llenvmanager.h"
#include "lldaycyclemanager.h"

// Camera
#include "llfocusmgr.h"
#include "llvoavatarself.h"
#include "llagentcamera.h"

//raycast globals
BOOL eAPI::ignoreAgents = FALSE;

//ghost params needs work.
BOOL ghosting = FALSE;
LLUUID eAPI::ghost_key("");

LLVector3 ghost_root(0.f);
LLVector3 ghost_offset(0.f);

F32 ghost_dist = 0.0;
BOOL ghost_rot = FALSE;

LLVector3 ghost_pos_org(0.f);
LLQuaternion ghost_rot_org;

//LLSD reattach;
LLSD attach_again;
BOOL reattach = FALSE;


eAPI::eAPI()
:	LLEventTimer(5.f)
{
	stopTimer();
}

eAPI::~eAPI()
{
}

void eAPI::startTimer()
{
	mEventTimer.stop();
	mEventTimer.start();
}

void eAPI::stopTimer()
{
	mEventTimer.stop();
}

BOOL eAPI::tick()
{
	if(reattach)
	{
		reattach = FALSE;
		U32 i;
		U32 len = attach_again.size();
		for(i = 0; i < len; ++i)
		{
			LLAppearanceMgr::instance().wearItemOnAvatar(LLUUID(attach_again[i].asString()));
		}
		attach_again.clear();
	}
	stopTimer();
	return FALSE;
}

BOOL eAPI::isEnabled()
{
	return gSavedSettings.getBOOL("ExodusAPI");
}

void eAPI::dataStream(std::string data)
{
	if(!isEnabled())return;
	sendResponse(gAgent.getID(),-222222,"#datastream",data);
}

void eAPI::idleCall()
{
	if(ghosting)mouseRaycast(-1,256.0,ghosting,"cast_idle",ghost_key);
}

BOOL eAPI::parseSounds(LLUUID sound_id, LLUUID source_id, LLUUID source_owner_id)
{
	if(source_owner_id != gAgent.getID() || source_owner_id == source_id)return FALSE;
	if(sound_id == LLUUID("a128c28c-b990-514e-218c-c1527511c854"))
	{
		sendResponse(source_id,-222222,"#version,0.1",llformat("%d,%d",(LLStartUp::getStartupState() < STATE_STARTED),isEnabled()));
		return TRUE;
	}
	return FALSE;
}

// Chat given from scripts and chat is handled here:
BOOL eAPI::phraseChat(std::string input, LLViewerObject* source_obj, LLUUID source_id)
{
	if(input.length() < 4)return FALSE;
	if(!isEnabled())return FALSE;
	if(input[0] != EAPI_CMD_PREFIX)return FALSE;

	BOOL debug = gSavedSettings.getBOOL("ExodusAPIDebug");
	BOOL passToChat = !gSavedSettings.getBOOL("ExodusAPIPassToChat");
	BOOL agent = (gAgent.getID() == source_id) ? TRUE : FALSE;

	//#pragma region parse chat
	LLSD cmd;
	U32 count = 0;
	std::string token;
	std::istringstream iss(input);
	while(getline(iss, token, ',')){ cmd[count] = token; count += 1; }

	U32 cmdLength = cmd.size();
	std::string command = cmd[0].asString();
	//#pragma endregion

	//#pragma region Version Commands
	if(command == "#eapi")
	{
		if(cmdLength == 2)sendResponse(source_id,cmd[1].asInteger(),command,"0.1");//Proper versioning plz
		else eAPIDebug(debug,command,"Format '#eapi,<chanel number>' expected.");
		return passToChat;
	}
	//#pragma endregion
	//#pragma region Object Inventory Commands
	else if(command == "#loadinv") // || command == "#load")
	{
		if(LLStartUp::getStartupState() >= STATE_STARTED && source_obj) source_obj->requestInventory();
		return passToChat;
	}
	else if(command == "#rez" && !agent)
	{
		if(cmdLength == 5)
		{
			std::string inv_name = cmd[1].asString();

			F32 x, z;
			F32 y = cmd[3].asReal();
			std::string temp;

			if((temp = cmd[2].asString()) != "" && temp[0] == '<') x = atof(temp.substr(1).c_str());
			else x = cmd[2].asReal();
				
			if((temp = cmd[4].asString()) != "" && temp[temp.length() - 1] == '>') z = atof(temp.substr(0, temp.length() - 1).c_str());
			else z = cmd[4].asReal();

			LLViewerObject* from_object = gObjectList.findObject(source_id); // why is this here? isnt the object already passed to the function?

			if(from_object)
			//if(source_obj)
			{
				LLInventoryObject::object_list_t obj_inv;
				from_object->getInventoryContents(obj_inv);
				//source_obj->getInventoryContents(obj_inv);
				if(!obj_inv.empty())
				{
					LLVector3 target = LLVector3(llclamp(x, 0.f, 256.f), llclamp(y, 0.f, 256.f), llclamp(z, 0.f, 256.f));
					LLInventoryObject::object_list_t::iterator obj_it;
					for(obj_it = obj_inv.begin(); obj_it != obj_inv.end(); ++obj_it)
					{
						LLInventoryItem* inventory = (LLInventoryItem*)((LLInventoryObject*)(*obj_it));
						if(inventory->getName() == inv_name) //Do a Type check?
						{
							LLPermissions perms = inventory->getPermissions();

							U32 group_mask		= perms.getMaskGroup();
							U32 everyone_mask	= perms.getMaskEveryone();
							U32 next_owner_mask = perms.getMaskNextOwner();

							LLMessageSystem *msg = gMessageSystem;

							msg->newMessageFast(_PREHASH_RezObject);
							msg->nextBlockFast(_PREHASH_AgentData);
							msg->addUUIDFast(_PREHASH_AgentID,  gAgent.getID());
							msg->addUUIDFast(_PREHASH_SessionID,  gAgent.getSessionID());
							msg->addUUIDFast(_PREHASH_GroupID, gAgent.getGroupID());
							msg->nextBlock("RezData");
							msg->addUUIDFast(_PREHASH_FromTaskID, source_id);
							msg->addU8Fast(_PREHASH_BypassRaycast, (U8)TRUE);
							msg->addVector3Fast(_PREHASH_RayStart, target);
							msg->addVector3Fast(_PREHASH_RayEnd, target);
							msg->addUUIDFast(_PREHASH_RayTargetID, LLUUID::null);
							msg->addBOOLFast(_PREHASH_RayEndIsIntersection, FALSE);
							msg->addBOOLFast(_PREHASH_RezSelected, FALSE);
							msg->addBOOLFast(_PREHASH_RemoveItem, FALSE);
							msg->addU32Fast(_PREHASH_ItemFlags, inventory->getFlags());
							msg->addU32Fast(_PREHASH_GroupMask, group_mask);
							msg->addU32Fast(_PREHASH_EveryoneMask, everyone_mask);
							msg->addU32Fast(_PREHASH_NextOwnerMask, next_owner_mask);
							msg->nextBlockFast(_PREHASH_InventoryData);
							inventory->packMessage(msg);

							gAgent.sendReliableMessage();
						}
					}
				}
			}
		}
		else eAPIDebug(debug,command,"Format '#rez,<object name>,<vector position>' expected.");
		return passToChat;
	}
	//#pragma endregion
	//#pragma region Avatar Commands
	else if(command == "#sit")
	{
		LLUUID sitKey;
		if(cmdLength == 2)sitKey = LLUUID(cmd[1].asString());
		else if(cmdLength == 1 && !agent) sitKey == source_id;
		else
		{
			eAPIDebug(debug,command,"Incorrect number of parameters.");
			return passToChat;
		}

		eAPISit(sitKey);

		return passToChat;
	}
	else if(command == "#unsit")
	{
		gAgent.setControlFlags(AGENT_CONTROL_STAND_UP);
		return passToChat;
	}
	else if(command == "#groundsit")
	{
		gAgent.sitDown();
		return passToChat;
	}
	else if(command == "#touch")
	{
		LLUUID touchKey;
		if(cmdLength == 2)touchKey = LLUUID(cmd[1].asString());
		else if(cmdLength == 1 && !agent) touchKey = source_id;
		else
		{
			eAPIDebug(debug,command,"Incorrect number of parameters.");
			return passToChat;
		}

		LLViewerObject* touchObject = gObjectList.findObject(touchKey);

		if(touchObject) eAPITouch(touchObject);
		else eAPIDebug(debug, command,"Object with key : " + touchKey.asString() + " not found");

		return passToChat;
	}
	else if(command == "#getavatar")
	{
		if(cmdLength > 2)
		{
			S32 channel = cmd[1].asInteger();
			std::string resp = "";
			std::string data;
			std::string add;
			U32 index;
			for(index = 2; index < cmdLength; ++index)
			{
				data = cmd[index].asString();
				add = "";
				if(data == "group")add = gAgent.getGroupID().asString();
				else if(data == "avoffset")add = llformat("%f",gSavedPerAccountSettings.getF32("ExodusAvatarOffset"));
				else if(data == "clientao")add = (gSavedPerAccountSettings.getBOOL("ExodusAOEnable")) ? "1" : "0";
				else if(data == "fps") add = llformat("%d", (S32)LLViewerStats::getInstance()->mFPSStat.getMeanPerSec());
				else if(data == "ping") add = llformat("%d", (S32)LLViewerStats::getInstance()->mSimPingStat.getPrev(0));
				//else if(data == "windowsize") add = llformat("%d,%d",gViewerWindow->getWindowDisplayHeight();
				//else if(data == "bakealpha") add = (gSavedSettings.getBOOL("ExodusSkinAlpha")) ? "1" : "0";

				if(add.length() > 0) resp += ((resp.length() > 0)? "," : "" ) + add;
			}
			if(resp.length() > 0)sendResponse(source_id,channel,command,resp);
		}
		else eAPIDebug(debug, command,"Format '#getavatar,<channel number>,<setting>,<setting2>,<sett...' expected.");
		return passToChat;
	}
	else if(command == "#setavatar")
	{
		if(cmdLength == 3)
		{
			std::string data = cmd[1].asString();
			if(data == "avoffset")
			{
				F32 av_offset = cmd[2].asReal();
				if(av_offset < -2.0)av_offset = -2.0;
				if(av_offset > 2.0)av_offset = 2.0;
				gSavedPerAccountSettings.setF32("ExodusAvatarOffset",av_offset);
			}
			else if(data == "clientao")
			{
				BOOL client_ao = (cmd[2].asInteger()) ? TRUE : FALSE;
				gSavedPerAccountSettings.setBOOL("ExodusAOEnable", client_ao);
			}
			else if(data == "group")
			{
				LLUUID group_id(cmd[2].asString());
				if(gAgent.isInGroup(group_id) && group_id != gAgent.getGroupID())
				{
					LLMessageSystem* msg = gMessageSystem;
					msg->newMessageFast(_PREHASH_ActivateGroup);
					msg->nextBlockFast(_PREHASH_AgentData);
					msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
					msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
					msg->addUUIDFast(_PREHASH_GroupID, group_id);
					gAgent.sendReliableMessage();
				}
			}
			/*else if(data == "bakealpha")
			{
				BOOL bake_alpha = (cmd[2].asInteger()) ? TRUE : FALSE;
				gSavedSettings.setBOOL("ExodusSkinAlpha", bake_alpha);
			}*/
		}
		else eAPIDebug(debug, command,"Format '#setavatar,<setting>,<data>' expected.");
		return passToChat;
	}
	else if(command == "#stopanimations")
	{
		gAgent.stopCurrentAnimations();
		return passToChat;
	}
	else if(command == "#reattach" && !agent)
	{
		LLViewerObject* attachment = gObjectList.findObject(source_id);
		LLUUID attach_id = attachment->getAttachmentItemID();
		if(attachment->isAttachment())
		{
			LLAppearanceMgr::instance().removeItemFromAvatar(attach_id);
//			LLSingleton::getInstance()->startTimer();
			reattach = TRUE;
			attach_again[attach_again.size()] = attach_id;
		}
		return passToChat;
	}
	/*else if(command == "#rebake") FAIL
	{
		gAgentAvatarp->forceBakeAllTextures(TRUE);
		return passToChat;
	}*/
	//#pragma endregion
	//#pragma region Object Commands (COMPLETELY UNFINISHED)
	/*else if(command == "#lol")
	{
		std::string type = cmd[1].asString();
		if(type == "size" && cmdLength == 5)
		{
			lolsc = source_obj->getScale();

			std::string temp;

			F32 x = ((temp = cmd[2].asString()) != "" && temp[0] == '<') ? atof(temp.substr(1).c_str()) : cmd[2].asReal();
			F32 y = cmd[3].asReal();
			F32 z = ((temp = cmd[4].asString()) != "" && temp[temp.length() - 1] == '>') ? atof(temp.substr(0, temp.length() - 1).c_str()) : cmd[4].asReal();

			LLViewerObject* lolObj = gObjectList.findObject(source_id);
			if(lolObj)
			{
				lolObj->setScale(LLVector3(x,y,z));
			}
		}
		return passToChat;
	}
	else if(command == "#dupe")
	{
		if(cmdLength != 4)return passToChat;
		std::string temp;
		
		F32 x = ((temp = cmd[1].asString()) != "" && temp[0] == '<') ? atof(temp.substr(1).c_str()) : cmd[2].asReal();
		F32 y = cmd[2].asReal();
		F32 z = ((temp = cmd[3].asString()) != "" && temp[temp.length() - 1] == '>') ? atof(temp.substr(0, temp.length() - 1).c_str()) : cmd[4].asReal();

		LLViewerObject* lolObj = gObjectList.findObject(source_id);
		eAPIDebug(TRUE,command,"CREATE NEW");
		LLViewerObject* newObj(gObjectList.createObjectViewer(lolObj->getPCode(),lolObj->getRegion()));
		eAPIDebug(TRUE,command,"FIND NEW");
		LLViewerObject* arfObj = gObjectList.findObject(newObj->getID());
		eAPIDebug(TRUE,command,"NEW == " + arfObj->getID().asString());
		if(arfObj)
		{
			arfObj->setPosition(lolObj->getPosition() + LLVector3(x,y,z));
		}
		return passToChat;
	}*/
	else if(command == "#ghost")
	{
		if(cmdLength > 2)
		{
			S32 channel = cmd[1].asInteger();
			std::string type = cmd[2].asString();
			if(type == "start")
			{
				if(ghosting)sendResponse(source_id,channel,command,"ALREADY_GHOSTING");
				else
				{
					LLViewerObject* ghostObject = gObjectList.findObject(source_id);
					if(ghostObject && ghostObject->isRoot())
					{
						
						if(cmdLength == 11)
						{
							ghost_key = source_id;
							ghost_pos_org = ghostObject->getPosition();
							ghost_rot_org = ghostObject->getRotation();
						
							ghosting = TRUE;

							std::string temp;
							F32 x = ((temp = cmd[3].asString()) != "" && temp[0] == '<') ? atof(temp.substr(1).c_str()) : cmd[3].asReal();
							F32 y = cmd[4].asReal();
							F32 z = ((temp = cmd[5].asString()) != "" && temp[temp.length() - 1] == '>') ? atof(temp.substr(0, temp.length() - 1).c_str()) : cmd[5].asReal();
							ghost_root = LLVector3(x,y,z);

							x = ((temp = cmd[6].asString()) != "" && temp[0] == '<') ? atof(temp.substr(1).c_str()) : cmd[6].asReal();
							y = cmd[7].asReal();
							z = ((temp = cmd[8].asString()) != "" && temp[temp.length() - 1] == '>') ? atof(temp.substr(0, temp.length() - 1).c_str()) : cmd[8].asReal();

							ghost_offset = LLVector3(x,y,z);

							ghost_dist = cmd[9].asReal();
							ghost_rot = (cmd[10].asInteger()) ? TRUE : FALSE;
						}
						else eAPIDebug(debug,command,"Incorrect number of parameters");
					}
					else eAPIDebug(debug, command, "unable to find a root prim object with key : " + source_id.asString());
				}
			}
			else if(type == "finish")
			{
				LLViewerObject* ghostObject = gObjectList.findObject(ghost_key);
				if(ghostObject && ghosting)
				{
					LLVector3 newpos = ghostObject->getPosition();
					LLQuaternion newrot = ghostObject->getRotation();

					std::string resp = llformat("<%f,%f,%f>,<%f,%f,%f,%f>",
										newpos.mV[0], newpos.mV[1], newpos.mV[2],
										newrot.mQ[0], newrot.mQ[1], newrot.mQ[2], newrot.mQ[3]);

					resp += "," + ghost_key.asString();

					sendResponse(ghost_key,channel,"#ghost",resp);

					ghostObject->setPosition(ghost_pos_org);
					ghostObject->setRotation(ghost_rot_org);
				}
				ghostStop();
			}
			else if(type == "cancel")
			{
				LLViewerObject* ghostObject = gObjectList.findObject(ghost_key);
				if(ghostObject && ghosting)
				{
					ghostObject->setPosition(ghost_pos_org);
					ghostObject->setRotation(ghost_rot_org);
				}
				ghostStop();
			}
		}
		return passToChat;
	}
	/* feature needs work fixing alphas
	else if(command == "#bakedtextures")
	{
		if(cmdLength == 4)
		{
			LLViewerObject* from_object = gObjectList.findObject(source_id); // again doesnt the function already have this?
			if(from_object)
			{
				from_object->setTEImage(cmd[1].asInteger(),
					LLViewerTextureManager::getFetchedTexture(
						gAgentAvatarp->getTE(LLVOAvatarDefines::TEX_HEAD_BAKED)->getID(), TRUE, LLViewerTexture::BOOST_NONE, LLViewerTexture::LOD_TEXTURE));

				from_object->setTEImage(cmd[2].asInteger(),
					LLViewerTextureManager::getFetchedTexture(
						gAgentAvatarp->getTE(LLVOAvatarDefines::TEX_UPPER_BAKED)->getID(), TRUE, LLViewerTexture::BOOST_NONE, LLViewerTexture::LOD_TEXTURE));

				from_object->setTEImage(cmd[3].asInteger(),
					LLViewerTextureManager::getFetchedTexture(
						gAgentAvatarp->getTE(LLVOAvatarDefines::TEX_LOWER_BAKED)->getID(), TRUE, LLViewerTexture::BOOST_NONE, LLViewerTexture::LOD_TEXTURE));

				from_object->sendTEUpdate();
			}
		}
		return TRUE;
	}
	*/
	//#pragma endregion
	//#pragma region Raycast Commands
	else if(command == "#rc")
	{
		if(cmdLength > 2)
		{
			S32 channel = cmd[1].asInteger();
			std::string rctype = cmd[2].asString();
			if(rctype == "once")
			{
				F32 depth = ((cmdLength > 3) ? cmd[3].asReal() : 256.f);
				BOOL ignoreagents = (cmdLength > 4 && cmd[4].asInteger()) ? TRUE : FALSE;
				std::string label = (cmdLength > 5) ? cmd[5].asString() : "once";
				
				mouseRaycast(channel,depth,ignoreagents,label,source_id);
			}
			else if(rctype == "p2p")
			{
				if(cmdLength != 11)
				{
					eAPIDebug(debug, command,"Incorrect number of parameters");
					return !passToChat;
				}
				std::string temp;
				F32 x = ((temp = cmd[3].asString()) != "" && temp[0] == '<') ? atof(temp.substr(1).c_str()) : cmd[3].asReal();
				F32 y = cmd[4].asReal();
				F32 z = ((temp = cmd[5].asString()) != "" && temp[temp.length() - 1] == '>') ? atof(temp.substr(0, temp.length() - 1).c_str()) : cmd[5].asReal();
			
				LLVector3 start = LLVector3(x, y, z);
			
				x = ((temp = cmd[6].asString()) != "" && temp[0] == '<') ? atof(temp.substr(1).c_str()) : cmd[6].asReal();
				y = cmd[7].asReal();
				z = ((temp = cmd[8].asString()) != "" && temp[temp.length() - 1] == '>') ? atof(temp.substr(0, temp.length() - 1).c_str()) : cmd[8].asReal();
			
				LLVector3 target =  LLVector3(x, y, z);
				
				BOOL ignoreagents = (cmd[9].asInteger()) ? TRUE : FALSE;

				std::string label = cmd[10].asString();
			
				if(start != target)performRaycast(start,target,ignoreagents,source_id,channel,label);
			}
		}
		else eAPIDebug(debug, command,"Incorrect number of parameters");
		return passToChat;
	}
	//#pragma endregion
	//#pragma region Windlight commands
	else if(command == "#wl")
	{
		if(cmdLength == 2 || cmdLength == 3)
		{
			std::string wltype = cmd[1].asString();
			if(wltype == "load") LLEnvManagerNew::instance().usePrefs();
			else if(wltype == "clear") LLEnvManagerNew::instance().usePrefs();
			else if(cmdLength == 3)
			{
				RlvWindLight::instance().setValue(cmd[1], cmd[2], true); // Need to change this. Dont want to leave in RLV dependancies.
			}
		}
		else eAPIDebug(debug, command,"Incorrect number of parameters");
		return passToChat;
	}
	//#pragma endregion
	//#pragma region Encryption commands (COMPLETELY UNFINISHED)
	/*
	else if(command == "#xor64Encrypt")
	{
		eAPIDebug(debug,command,"is not implemented yet");
		return passToChat;
	}
	else if(command == "#xor64decrypt")
	{
		eAPIDebug(debug,command,"is not implemented yet");
		return passToChat;
	}
	else if(command == "#storeencryptionkey")
	{
		if(cmdLength == 3)
		{
			std::string key_label = cmd[1].asString();
			std::string key_to_store = cmd[2].asString();
			eAPIDebug(debug,command,"is not implemented yet. (recieved label : " + key_label + " , and key : " + key_to_store + ")");
		}
		else eAPIDebug(debug,command,"Incorrect number of parameters");
		return passToChat;
	}
	*/
	//#pragma endregion

	eAPIDebug(debug,command,"Not recognized or we have missed a return");
	return FALSE;
}


void eAPI::eAPIDebug(BOOL debug, std::string command, std::string debug_message)
{
	if(!debug)return;
	if(command.empty()) return;
	if(debug_message.empty()) return;

	LLChat chat;

	chat.mText = "eAPI-ERROR : " + command + " : " + debug_message;
	chat.mSourceType = CHAT_SOURCE_SYSTEM;

	LLSD args;
	args["type"] = LLNotificationsUI::NT_NEARBYCHAT;

	LLNotificationsUI::LLNotificationManager::instance().onChat(chat, args);
}

void eAPI::sendResponse(LLUUID target, S32 channel, std::string command, std::string response)
{
	if(command.empty()) return;
	if(response.empty()) return;
	if(channel == 222222)response = "channel reserved for eAPIDataStream";
	std::string message = command + "," + response;
	LLMessageSystem	*msg = gMessageSystem;

	msg->newMessage("ScriptDialogReply");
	msg->nextBlock("AgentData");
	msg->addUUID("AgentID", gAgent.getID());
	msg->addUUID("SessionID", gAgent.getSessionID());
	msg->nextBlock("Data");
	msg->addUUID("ObjectID", target);
	msg->addS32("ChatChannel", channel);
	msg->addS32("ButtonIndex", 1);
	msg->addString("ButtonLabel", /*llformat("%d,", gFrameCount) + */message);
	gAgent.sendReliableMessage();
}

void eAPI::eAPISit(LLUUID target)
{
	LLMessageSystem	*msg = gMessageSystem;

	msg->newMessageFast(_PREHASH_AgentRequestSit);
	msg->nextBlockFast(_PREHASH_AgentData);
	msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
	msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
	msg->nextBlockFast(_PREHASH_TargetObject);
	msg->addUUIDFast(_PREHASH_TargetID, target);
	msg->addVector3Fast(_PREHASH_Offset, LLVector3::zero);

	gAgent.sendReliableMessage();
}

void eAPI::eAPITouch(LLViewerObject* touchObject)
{
	LLMessageSystem	*msg = gMessageSystem;
	msg->newMessageFast(_PREHASH_ObjectGrab);
	msg->nextBlockFast( _PREHASH_AgentData);
	msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
	msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
	msg->nextBlockFast( _PREHASH_ObjectData);
	msg->addU32Fast(_PREHASH_LocalID, touchObject->mLocalID);
	msg->addVector3Fast(_PREHASH_GrabOffset, LLVector3::zero );
	msg->nextBlock("SurfaceInfo");
	msg->addVector3("UVCoord", LLVector3::zero);
	msg->addVector3("STCoord", LLVector3::zero);
	msg->addS32Fast(_PREHASH_FaceIndex, 0);
	msg->addVector3("Position", touchObject->getPosition());
	msg->addVector3("Normal", LLVector3::zero);
	msg->addVector3("Binormal", LLVector3::zero);
	gAgent.sendReliableMessage();//Because the line bellow doesnt work, and i cba see why
	//msg->sendMessage( touchObject->getRegion()->getHost()); //Issue here
			
	// *NOTE: Hope the packets arrive safely and in order or else
	// there will be some problems.
	// *TODO: Just fix this bad assumption.
			
	msg->newMessageFast(_PREHASH_ObjectDeGrab);
	msg->nextBlockFast(_PREHASH_AgentData);
	msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
	msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
	msg->nextBlockFast(_PREHASH_ObjectData);
	msg->addU32Fast(_PREHASH_LocalID, touchObject->mLocalID);
	msg->nextBlock("SurfaceInfo");
	msg->addVector3("UVCoord", LLVector3::zero);
	msg->addVector3("STCoord", LLVector3::zero);
	msg->addS32Fast(_PREHASH_FaceIndex, 0);
	msg->addVector3("Position", touchObject->getPosition());
	msg->addVector3("Normal", LLVector3::zero);
	msg->addVector3("Binormal", LLVector3::zero);
	gAgent.sendReliableMessage();//Because the line bellow doesnt work, and i cba see why
	//msg->sendMessage(touchObject->getRegion()->getHost());	//Issue here
}

void eAPI::mouseRaycast(S32 channel, F32 depth, BOOL ignoreagents, std::string label, LLUUID source_id)
{
	S32 x = gViewerWindow->getCurrentMouseX();
	S32 y = gViewerWindow->getCurrentMouseY();
	
	LLVector3 mouse_direction_global = gViewerWindow->mouseDirectionGlobal(x, y);
	LLVector3 mouse_point_global = LLViewerCamera::getInstance()->getOrigin();
	LLVector3 n = LLViewerCamera::getInstance()->getAtAxis();
	LLVector3 p = mouse_point_global + n * LLViewerCamera::getInstance()->getNear();
	LLVector3 target;
	line_plane(mouse_point_global, mouse_direction_global, p, n, target);
	LLVector3 start = target + mouse_direction_global * depth;

	if(start != target)performRaycast(target,start,ignoreagents,source_id,channel,label);
}

void eAPI::performRaycast(LLVector3 target, LLVector3 source, BOOL ignoreagents, LLUUID source_id,S32 channel, std::string label)
{
	S32 castFace = -1;
	LLVector3 castIntersection = LLVector3(0.f);
	LLVector2 castTextureCoord;
	LLVector3 castNormal;
	LLVector3 castBinormal;

	if(source_id.isNull()) source_id = gAgent.getID();

	LLViewerObject* castObject = NULL;
	/*
	castObject = magic ?
		gPipeline.lineSegmentIntersectInWorld(target, source, FALSE, &castFace, &castIntersection, &castTextureCoord, &castNormal, &castBinormal)
		:
		gPipeline.lineSegmentIntersectInWorldObjectsOnly(target, source, FALSE, &castFace, &castIntersection, &castTextureCoord, &castNormal, &castBinormal);
	*/
	ignoreAgents = ignoreagents;
	castObject = gPipeline.lineSegmentIntersectInWorld(target, source, FALSE, &castFace, &castIntersection, &castTextureCoord, &castNormal, &castBinormal);
	ignoreAgents = FALSE;
	LLVector3 translate = castIntersection;
	
	std::string resp = "";
	
	if(castObject)
	{
		LLUUID castKey = castObject->getID();
		
		LLCoordFrame orient;
		orient.lookDir(castNormal, castBinormal);
		LLQuaternion rotation = orient.getQuaternion();
		std::string key = castKey.asString();

		//gesture only based raycast touching and sitting.
		if(label == "cast_touch")
		{
			eAPITouch(castObject);
			return;
		}
		else if(label == "cast_sit")
		{
			eAPISit(castObject->getID());
			return;
		}
		else if(label == "cast_idle")
		{
			if(ghosting)ghostObject(translate,rotation);
			return;
		}

		if(source_id.isNull()) castObject->getID();

		resp = llformat(
			"<%f,%f,%f>,<%f,%f,%f,%f>,",
			translate.mV[0], translate.mV[1],translate.mV[2],
			rotation.mQ[0], rotation.mQ[1], rotation.mQ[2], rotation.mQ[3]) +
			key;
	}
	else
	{
		if(!translate.isNull())
		{
			LLCoordFrame orient;
			orient.lookDir(castNormal, castBinormal);
			LLQuaternion rotation = orient.getQuaternion();

			resp =  llformat(
				"<%f,%f,%f>,<%f,%f,%f,%f>,",
				translate.mV[0], translate.mV[1],translate.mV[2],
				rotation.mQ[0], rotation.mQ[1], rotation.mQ[2], rotation.mQ[3]) +
				"null";
		}
		else resp = "null,null,null";
	}

	sendResponse(source_id,channel,"#rc",resp + "," + label);
}

S32 tick = 0;

void eAPI::ghostObject(LLVector3 pos, LLQuaternion rot)
{
	if(!ghosting)return;
	LLViewerObject* ghostObject = gObjectList.findObject(ghost_key);
	if(!ghostObject)
	{
		ghostStop();
		return;
	}
	if(dist_vec(pos, ghost_root) > ghost_dist)
	{
		LLVector3 dir = pos - ghost_root;
		F32 mag = (F32) sqrt(dir[0]*dir[0] + dir[1]*dir[1] + dir[2]*dir[2]);
		dir = dir / mag;
		dir *= ghost_dist;
		pos = ghost_root + dir;
	}
	ghostObject->setPosition(pos + (ghost_offset * rot));
	if(ghost_rot)ghostObject->setRotation(rot);
}

void eAPI::ghostStop()
{
	ghosting = FALSE;
	ghost_key = LLUUID("");

	ghost_root = LLVector3(0.f);
	ghost_offset = LLVector3(0.f);

	ghost_dist = 0.0;
	ghost_rot = FALSE;

	ghost_pos_org = LLVector3(0.f);
	//ghost_rot_org = LLQuaternion(0.f);
}

/*		else if(message.substr(0, 4) == "#wl:")
		{
			if(!gSavedSettings.getBOOL("ExodusGenesisAPI")) return TRUE;

			if(message == "#wl:save")
			{
				// Apparently this isn't needed anymore?
				//LLEnvManagerNew::instance().saveUserPrefs();
			}
			else if(message == "#wl:load")
			{
				LLEnvManagerNew::instance().usePrefs();
			}
			else if(message == "#wl:clear")
			{
				LLEnvManagerNew::instance().usePrefs();
			}
			else
			{
				LLSD args;
				U32 count = 0;
				std::string token;
				std::istringstream iss(message.substr(4));
				
				while(getline(iss, token, '=')){ args[count] = token; ++count; }

				if(count > 1)
				{
					RlvWindLight::instance().setValue(args[0], args[1], true);
				}
			}

			return TRUE;
		}
	}

	return FALSE;
}
*/