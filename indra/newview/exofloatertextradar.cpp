/** 
 * @file exoFloaterTextRadar.cpp
 * @brief The alternative lightweight statistics window.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "message.h"

#include "llfloaterreg.h"
#include "exofloatertextradar.h"

#include "llagent.h"
#include "llappviewer.h"
#include "llavatarnamecache.h"
#include "llbutton.h"
#include "lltextbox.h"
#include "llviewercontrol.h"
#include "llviewerstats.h"
#include "llviewerregion.h"
#include "llworld.h"

#include "llpanelpeople.h"

// This integer sets order.
int FloaterTextRadarOrder; // Todo: Add feature to set this from debug setting.

// exodus_floater_text_radar.xml

exoFloaterTextRadar::exoFloaterTextRadar(const LLSD& key) :
	LLFloater(key),
	LLEventTimer(0.1f)
{
	mEventTimer.stop();
}

// Todo: Resize draggable area to number of lines of text?

BOOL exoFloaterTextRadar::postBuild()
{
	mText = getChild<LLTextBox>("radar_text");
	mText->setHAlign((LLFontGL::HAlign)gSavedSettings.getS32("ExodusTextRadarHAlign"));
	mText->setText(LLStringExplicit("There seems to be nobody nearby..."));
	mText->setDoubleClickCallback(boost::bind(&exoFloaterTextRadar::onDoubleClick, this, _1, _2, _3, _4));

	getChild<LLButton>("align_left")->setCommitCallback(boost::bind(&exoFloaterTextRadar::textAlignLeft, this));
	getChild<LLButton>("align_center")->setCommitCallback(boost::bind(&exoFloaterTextRadar::textAlignCenter, this));
	getChild<LLButton>("align_right")->setCommitCallback(boost::bind(&exoFloaterTextRadar::textAlignRight, this));
	
	windowSize = getRect();
	renderBackground = gSavedSettings.getBOOL("ExodusTextRadarBackground");
	if (!renderBackground)
	{
		LLRect textSize = mText->calcScreenRect();
		textSize.setCenterAndSize(textSize.getCenterX() - 10, textSize.getCenterY(), textSize.getWidth(), textSize.getHeight() + 20);

		setRect(textSize);

		setCanResize(FALSE);
		setCanClose(FALSE);
	}

	mEventTimer.start();

	return TRUE;
}

struct avatar_vec
{
    avatar_vec() : key(), position() {}

    avatar_vec(LLUUID key, LLVector3d position) :
		key(key), position(position) {}

    bool operator<(avatar_vec const& req) const
		{ return position.magVec() < req.position.magVec(); }

	LLUUID key;
	LLVector3d position;
};

BOOL exoFloaterTextRadar::tick()
{
	std::string displayText;

	static LLCachedControl<F32> radar_range(gSavedSettings, "ExodusRadarRange", 4096.f);
	static LLCachedControl<S32> text_radar_limit(gSavedSettings, "ExodusTextRadarLimit", 20);

	S32 textLimit = text_radar_limit;

	const LLVector3d& myPosition = gAgent.getPositionGlobal();
	
	uuid_vec_t avatar_ids;
	std::vector<LLVector3d> positions;
	LLWorld::getInstance()->getAvatars(&avatar_ids, &positions, myPosition, radar_range);

	int length = avatar_ids.size();
	if (length)
	{
		std::vector<avatar_vec> avatars;
		for(int i = 0; i < length; i++)
		{
			avatars.push_back(avatar_vec(avatar_ids[i], positions[i]));
		}

		if (FloaterTextRadarOrder) std::sort(avatars.begin(), avatars.end()); //Furthest first. 
		else std::sort(avatars.rbegin(), avatars.rend()); //Closest first.
		for(int i = 0; i < length; i++)
		{
			if (--textLimit < 0) break;

			LLVector3d pos = avatars[i].position;
			if (pos.mdV[VZ] == 0.f) continue;
			std::string distanceText = llformat("%3.1fm", dist_vec(pos, myPosition));

			LLUUID id = avatars[i].key;
			std::string targetName = "Loading...";
			LLAvatarName avatarName;
			if (LLAvatarNameCache::get(id, &avatarName)) targetName = avatarName.getCompleteName();

			displayText.append(targetName + " (" + distanceText + ")\n");
		}

		if (displayText.empty()) displayText = "There seems to be nobody nearby...";
	}
	else displayText = "There seems to be nobody nearby...";


	mText->setText(displayText);

	return FALSE;
}

void exoFloaterTextRadar::draw()
{
	if (renderBackground)
	{
		LLFloater::draw();
	}
	else if (mText->getVisible() && mText->getRect().isValid()) // Render just the text...
	{
		LLView* rootp = LLUI::getRootView();
		LLRect screen_rect = mText->calcScreenRect();

		if (rootp->getLocalRect().overlaps(screen_rect) && LLUI::sDirtyRect.overlaps(screen_rect))
		{
			LLUI::pushMatrix();
			LLUI::translate((F32)mText->getRect().mLeft, (F32)mText->getRect().mBottom, 0.f);

			mText->draw();

			LLUI::popMatrix();
		}
	}
}

BOOL exoFloaterTextRadar::onDoubleClick(LLUICtrl* ctrl, S32 x, S32 y, MASK mask)
{
	renderBackground = !renderBackground;

	gSavedSettings.setBOOL("ExodusTextRadarBackground", renderBackground);

	setCanResize(renderBackground);
	setCanClose(renderBackground);

	if (renderBackground)
	{
		LLRect windowPosition = getRect();
		windowSize.setCenterAndSize(windowPosition.getCenterX() + 10, windowPosition.getCenterY() + 13, windowSize.getWidth(), windowSize.getHeight());

		setRect(windowSize);
	}
	else
	{
		windowSize = getRect();

		LLRect textSize = mText->calcScreenRect();
		textSize.setCenterAndSize(textSize.getCenterX() - 10, textSize.getCenterY(), textSize.getWidth(), textSize.getHeight() + 20);

		setRect(textSize); 
	}

	return TRUE;
}

void exoFloaterTextRadar::textAlignLeft()
{
	gSavedSettings.setS32("ExodusTextRadarHAlign", (S32)LLFontGL::LEFT);

	mText->setHAlign(LLFontGL::LEFT);
}

void exoFloaterTextRadar::textAlignCenter()
{
	gSavedSettings.setS32("ExodusTextRadarHAlign", (S32)LLFontGL::HCENTER);

	mText->setHAlign(LLFontGL::HCENTER);
}

void exoFloaterTextRadar::textAlignRight()
{
	gSavedSettings.setS32("ExodusTextRadarHAlign", (S32)LLFontGL::RIGHT);

	mText->setHAlign(LLFontGL::RIGHT);
}
