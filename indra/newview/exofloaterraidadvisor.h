/** 
 * @file exofloaterraidadvisor.h
 * @brief EXOFloaterRaidAdvisor class definition
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_FLOATER_RAID_ADVISOR
#define EXO_FLOATER_RAID_ADVISOR

#include "llfloater.h"
#include "lleventtimer.h"
#include "llassettype.h"
#include "llextendedstatus.h"

class LLInventoryItem;
class LLTextBox;
class LLScrollListCtrl;
class LLButton;
class LLVFS;

class exoFloaterRaidAdvisor :
	public LLFloater,
	public LLEventTimer
{
	friend class LLFloaterReg;

private:
	exoFloaterRaidAdvisor(const LLSD& key);
	~exoFloaterRaidAdvisor();

	BOOL postBuild();

	virtual BOOL tick();
	
	void onChangedSort();
	
	bool getData(std::string filename = "raid_advisor.ra.evon", bool local = true);
	bool getDataOld(std::string filename = "raid_advisor.xml", bool local = true);

	bool importData(LLSD data);

	bool saveData(std::string filename = "raid_advisor.ra.evon", bool local = true);

	void updateData();
	

	void removeItem();
	static void removeItemCallback(const LLSD& notification, const LLSD& response);

	void onOpenMap();

	void selectionChange();

	void exportData();
	static void exportDataCallback(const LLSD& notification, const LLSD& response);
	void exportFile(std::string filename);
	void exportNotecard(std::string filename);

	void importData();

	LLSD mapList;

	LLTextBox* textTitle;
	LLTextBox* textSubTitle;

	LLScrollListCtrl* simulatorList;

	LLButton* mapPreview;

	static void loadNotecard(LLVFS* vfs, const LLUUID& id, LLAssetType::EType type, void* userdata, S32 status, LLExtStat ext);
	static void importNotecardCallback(const LLSD& notification, const LLSD& response);
	
public:
	std::string loadNotecardName;

	LLUUID mInventoryFolder;

	void importNotecard(const LLInventoryItem* item);

	static void addItemCallback(const LLSD& notification, const LLSD& response);
};

#endif // EXO_FLOATER_RAID_ADVISOR
