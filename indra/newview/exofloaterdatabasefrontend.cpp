/** 
 * @file exofloaterdatabasefrontend.cpp
 * @brief The database frontend for managing the Ark group.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

// Todo: Maybe add timeout timer timers?

#include "llviewerprecompiledheaders.h"

#include "llfloaterreg.h"

#include "exofloaterdatabasefrontend.h"
#include "exosystems.h"

#include "llagent.h"
#include "llprogressbar.h"
#include "llhttpclient.h"
#include "llviewermedia.h"
#include "llversioninfo.h"
#include "llnotificationsutil.h"
#include "llsdserialize.h"
#include "llbufferstream.h"
#include "lltextbox.h"
#include "llspinctrl.h"
#include "llcombobox.h"
#include "llbutton.h"
#include "llviewercontrol.h"
#include "lllineeditor.h"
#include "llsechandler_basic.h"
#include "llavatarnamecache.h"
#include "llavatarname.h"
#include "llmd5.h"
#include "llfloatergroupinvite.h"

//
// Client
//

class exoDatabaseClient : public LLHTTPClient::Responder
{
public:
	exoDatabaseClient(LLUUID key, std::string type) :
		mTargetKey(key), mTargetType(type)
	{
	}

	LLUUID mTargetKey;
	std::string mTargetType;

	void completedRaw(U32 status, const std::string& reason, const LLChannelDescriptors& channels, const LLIOPipe::buffer_ptr_t& buffer)
	{
		LLSD content;
		LLBufferStream istr(channels, buffer.get());
		if (!LLSDSerialize::fromXML(content, istr)) status = 400;

		if (mTargetType == "ark_database_frontend")
		{
			exoFloaterDatabaseFrontend* target =
				LLFloaterReg::findTypedInstance<exoFloaterDatabaseFrontend>(mTargetType, mTargetKey);

			if (target) target->recieveData(content, isGoodStatus(status));
		}
		else if (mTargetType == "ark_database_frontend_adjust")
		{
			exoFloaterDatabaseFrontendAdjust* target =
				LLFloaterReg::findTypedInstance<exoFloaterDatabaseFrontendAdjust>(mTargetType, mTargetKey);

			if (target) target->recieveData(content, isGoodStatus(status));
		}
		else if (mTargetType == "ark_database_frontend_invite")
		{
			exoFloaterDatabaseFrontendInvite* target =
				LLFloaterReg::findTypedInstance<exoFloaterDatabaseFrontendInvite>(mTargetType, mTargetKey);

			if (target) target->recieveData(content, isGoodStatus(status));
		}
		else llwarns << "Unknown request type: " << mTargetType << llendl;
	}
};

//
// Load
//

// exodus_floater_database_frontend.xml

exoFloaterDatabaseFrontend::exoFloaterDatabaseFrontend(const LLSD& key) :
	LLFloater(key)
{
	mTargetKey = key.asUUID();
}

exoFloaterDatabaseFrontend::~exoFloaterDatabaseFrontend()
{
}

BOOL exoFloaterDatabaseFrontend::postBuild()
{
	if (exoSystems::databaseBackendLocation.empty())
	{
		LLFloater::closeFloater();
	}
	else
	{
		mProgressBar = getChild<LLProgressBar>("timeout_progress_bar");
		if (mProgressBar) mProgressBar->setValue(100.f);

		LLSD headers; // HTTP_*
		headers.insert("AGENT_VERSION", LLVersionInfo::getAgentVersion());
		headers.insert("AGENT_KEY", gAgent.getID());
		headers.insert("TARGET_KEY", mTargetKey.asString());
		headers.insert("REQUEST_TYPE", "GET");

		LLHTTPClient::get(exoSystems::databaseBackendLocation, new exoDatabaseClient(mTargetKey, "ark_database_frontend"), headers);
	}

	return TRUE;
}

void exoFloaterDatabaseFrontend::recieveData(LLSD data, bool status)
{
	if (status && data.has("valid"))
	{
		if (data.has("name"))
		{
			exoFloaterDatabaseFrontendAdjust* target =
				LLFloaterReg::getTypedInstance<exoFloaterDatabaseFrontendAdjust>("ark_database_frontend_adjust", LLSD(mTargetKey));

			if (target)
			{
				LLFloaterReg::showInstance("ark_database_frontend_adjust", LLSD(mTargetKey));

				target->center();
				target->setData(data["name"].asString(), data["rank"].asInteger(), data["balance"].asInteger(), data["division"].asString());
			}
		}
		else // ...no name would mean that they're not on the database!
		{
			exoFloaterDatabaseFrontendInvite* target =
				LLFloaterReg::getTypedInstance<exoFloaterDatabaseFrontendInvite>("ark_database_frontend_invite", mTargetKey);

			if (target)
			{
				LLFloaterReg::showInstance("ark_database_frontend_invite", mTargetKey);

				target->center();
			}
		}

		closeFloater();
	}
	else
	{
		LLNotificationsUtil::add("ExodusFrontendError", LLSD());

		closeFloater();
	}
}

//
// Adjust
//

// exodus_floater_database_frontend_adjust.xml

exoFloaterDatabaseFrontendAdjust::exoFloaterDatabaseFrontendAdjust(const LLSD& key) :
	LLFloater(key)
{
	mTargetKey = key.asUUID();
}

exoFloaterDatabaseFrontendAdjust::~exoFloaterDatabaseFrontendAdjust()
{
}

BOOL exoFloaterDatabaseFrontendAdjust::postBuild()
{
	LLPointer<LLCredential> databaseCred = gSecAPIHandler->loadCredential("DatabaseFrontend");

	mUsername = databaseCred->getIdentifier()["username"].asString();
	mPassword = databaseCred->getAuthenticator()["creds"].asString();

	getChild<LLLineEditor>("Username")->setValue(mUsername);
	getChild<LLLineEditor>("Password")->setValue(mPassword);

	getChild<LLButton>("Cancel")->setCommitCallback(boost::bind(&exoFloaterDatabaseFrontendAdjust::onClickClose, this));
	getChild<LLButton>("Submit")->setCommitCallback(boost::bind(&exoFloaterDatabaseFrontendAdjust::onClickSubmit, this));
	getChild<LLButton>("Delete")->setCommitCallback(boost::bind(&exoFloaterDatabaseFrontendAdjust::onClickDelete, this));

	return TRUE;
}

void exoFloaterDatabaseFrontendAdjust::setInputEnabled(bool enabled)
{
	setCanClose(enabled);

	getChild<LLView>("Cancel")->setEnabled(enabled);
	getChild<LLView>("Submit")->setEnabled(enabled);
	getChild<LLView>("Delete")->setEnabled(enabled);
	getChild<LLView>("Balance")->setEnabled(enabled);
	getChild<LLView>("Balance Adjustment")->setEnabled(enabled);
	getChild<LLView>("Rank")->setEnabled(enabled);
	getChild<LLView>("Division")->setEnabled(enabled);
	getChild<LLView>("Username")->setEnabled(enabled);
	getChild<LLView>("Password")->setEnabled(enabled);
}

void exoFloaterDatabaseFrontendAdjust::setData(std::string name, U32 rank, U32 balance, std::string division)
{
	getChild<LLTextBox>("Name")->setText(name);
	getChild<LLComboBox>("Rank")->setCurrentByIndex(rank);
	getChild<LLSpinCtrl>("Balance")->set(balance);
	getChild<LLComboBox>("Division")->selectByValue(division);
}

void exoFloaterDatabaseFrontendAdjust::onClickClose()
{
	closeFloater();
}

void exoFloaterDatabaseFrontendAdjust::onClickSubmit()
{
	setInputEnabled(false);
	
	std::string username = getChild<LLLineEditor>("Username")->getText();
	std::string password = getChild<LLLineEditor>("Password")->getText();

	if (password != mPassword || username != mUsername)
	{
		mUsername = username;
		mPassword = password;

		LLSD id = LLSD::emptyMap();
		id["type"] = "DatabaseFrontend";
		id["username"] = mUsername;

		LLSD auth = LLSD::emptyMap();
		auth["type"] = "DatabaseFrontend";
		auth["creds"] = mPassword;

		LLPointer<LLCredential> databaseCred = gSecAPIHandler->createCredential("DatabaseFrontend", id, auth);
		gSecAPIHandler->saveCredential(databaseCred, true);
	}
	
	password = mPassword + exoSystems::databaseBackendSalt;

	LLMD5 md5((const U8 *)password.c_str());
	char hash[33]; /* Flawfinder: ignore */
	md5.hex_digest(hash);

	LLSD headers; // HTTP_*
	headers.insert("AGENT_VERSION", LLVersionInfo::getAgentVersion());
	headers.insert("AGENT_KEY", gAgent.getID());
	headers.insert("TARGET_KEY", mTargetKey.asString());
	headers.insert("REQUEST_TYPE", "ADJUST");
	headers.insert("DATABASE_USERNAME", username);
	headers.insert("DATABASE_PASSWORD", hash);
	headers.insert("REQUEST_RANK", getChild<LLComboBox>("Rank")->getCurrentIndex());
	headers.insert("REQUEST_BALANCE", getChild<LLSpinCtrl>("Balance")->getValue().asInteger() + getChild<LLSpinCtrl>("Balance Adjustment")->getValue().asInteger());
	headers.insert("REQUEST_DIVISION", getChild<LLComboBox>("Division")->getValue());

	LLHTTPClient::get(exoSystems::databaseBackendLocation, new exoDatabaseClient(mTargetKey, "ark_database_frontend_adjust"), headers);
}

void exoFloaterDatabaseFrontendAdjust::onClickDelete()
{
	setInputEnabled(false);
	
	std::string username = getChild<LLLineEditor>("Username")->getText();
	std::string password = getChild<LLLineEditor>("Password")->getText();

	if (password != mPassword || username != mUsername)
	{
		mUsername = username;
		mPassword = password;

		LLSD id = LLSD::emptyMap();
		id["type"] = "DatabaseFrontend";
		id["username"] = mUsername;

		LLSD auth = LLSD::emptyMap();
		auth["type"] = "DatabaseFrontend";
		auth["creds"] = mPassword;

		LLPointer<LLCredential> databaseCred = gSecAPIHandler->createCredential("DatabaseFrontend", id, auth);
		gSecAPIHandler->saveCredential(databaseCred, true);
	}
	
	password = mPassword + exoSystems::databaseBackendSalt;

	LLMD5 md5((const U8 *)password.c_str());
	char hash[33]; /* Flawfinder: ignore */
	md5.hex_digest(hash);

	LLSD headers; // HTTP_*
	headers.insert("AGENT_VERSION", LLVersionInfo::getAgentVersion());
	headers.insert("AGENT_KEY", gAgent.getID());
	headers.insert("TARGET_KEY", mTargetKey.asString());
	headers.insert("REQUEST_TYPE", "DELETE");
	headers.insert("DATABASE_USERNAME", username);
	headers.insert("DATABASE_PASSWORD", hash);

	LLNotificationsUtil::add("ExodusFrontendRemove", LLSD(), headers, boost::bind(onConfirmDelete, _1, _2));
}

void exoFloaterDatabaseFrontendAdjust::onConfirmDelete(const LLSD& notification, const LLSD& response)
{
	if (LLNotificationsUtil::getSelectedOption(notification, response))
	{
		exoFloaterDatabaseFrontendAdjust* target =
			LLFloaterReg::findTypedInstance<exoFloaterDatabaseFrontendAdjust>("ark_database_frontend_adjust", notification["payload"]["TARGET_KEY"].asUUID());

		if (target)
		{
			target->setInputEnabled(true);
		}
		else llwarns << "Unable to find adjust instance for: " << notification["payload"]["TARGET_KEY"].asUUID() << llendl;
	}
	else LLHTTPClient::get(exoSystems::databaseBackendLocation, new exoDatabaseClient(notification["payload"]["TARGET_KEY"].asUUID(), "ark_database_frontend_adjust"), notification["payload"]);
}

void exoFloaterDatabaseFrontendAdjust::recieveData(LLSD data, bool status)
{
	if (status && data.has("valid"))
	{
		setCanClose(TRUE);
		closeFloater();
	}
	else
	{
		LLNotificationsUtil::add("ExodusFrontendError", LLSD());
		
		setCanClose(TRUE);
		closeFloater();
	}
}

//
// Invite
//

// exodus_floater_database_frontend_invite.xml

exoFloaterDatabaseFrontendInvite::exoFloaterDatabaseFrontendInvite(const LLSD& key) :
	LLFloater(key)
{
	mTargetKey = key.asUUID();
}

exoFloaterDatabaseFrontendInvite::~exoFloaterDatabaseFrontendInvite()
{
}

BOOL exoFloaterDatabaseFrontendInvite::postBuild()
{
	LLPointer<LLCredential> databaseCred = gSecAPIHandler->loadCredential("DatabaseFrontend");

	mUsername = databaseCred->getIdentifier()["username"].asString();
	mPassword = databaseCred->getAuthenticator()["creds"].asString();

	getChild<LLLineEditor>("Username")->setValue(mUsername);
	getChild<LLLineEditor>("Password")->setValue(mPassword);

	getChild<LLButton>("Cancel")->setCommitCallback(boost::bind(&exoFloaterDatabaseFrontendInvite::onClickClose, this));
	getChild<LLButton>("Submit")->setCommitCallback(boost::bind(&exoFloaterDatabaseFrontendInvite::onClickSubmit, this));
	getChild<LLButton>("Invite")->setCommitCallback(boost::bind(&exoFloaterDatabaseFrontendInvite::onClickInvite, this));

	setInputEnabled(false);

	return TRUE;
}

void exoFloaterDatabaseFrontendInvite::onOpen(const LLSD& key)
{
	LLAvatarNameCache::get(mTargetKey, boost::bind(&exoFloaterDatabaseFrontendInvite::onNameLoaded, _1, _2));
}

void exoFloaterDatabaseFrontendInvite::setInputEnabled(bool enabled)
{
	setCanClose(enabled);

	getChild<LLView>("Cancel")->setEnabled(enabled);
	getChild<LLView>("Submit")->setEnabled(enabled);
	getChild<LLView>("Invite")->setEnabled(enabled);
	getChild<LLView>("Group")->setEnabled(enabled);
	getChild<LLView>("Delete")->setEnabled(enabled);
	getChild<LLView>("Balance")->setEnabled(enabled);
	getChild<LLView>("Rank")->setEnabled(enabled);
	getChild<LLView>("Division")->setEnabled(enabled);
	getChild<LLView>("Username")->setEnabled(enabled);
	getChild<LLView>("Password")->setEnabled(enabled);
}

void exoFloaterDatabaseFrontendInvite::onNameLoaded(const LLUUID& id, const LLAvatarName av_name)
{
	exoFloaterDatabaseFrontendInvite* target =
		LLFloaterReg::findTypedInstance<exoFloaterDatabaseFrontendInvite>("ark_database_frontend_invite", id);

	if (target)
	{
		target->getChild<LLTextBox>("Name")->setText(av_name.getLegacyName());
		target->setInputEnabled(true);
	}
	else llwarns << "Unable to find invite instance for: " << id << llendl;
}

void exoFloaterDatabaseFrontendInvite::onClickClose()
{
	closeFloater();
}

void exoFloaterDatabaseFrontendInvite::onClickSubmit()
{
	std::string target = getChild<LLTextBox>("Name")->getText();
	if(target.empty() || target == "Loading...") return; // Todo: Display error/loading message?

	setInputEnabled(false);
	
	std::string username = getChild<LLLineEditor>("Username")->getText();
	std::string password = getChild<LLLineEditor>("Password")->getText();

	if (password != mPassword || username != mUsername)
	{
		mUsername = username;
		mPassword = password;

		LLSD id = LLSD::emptyMap();
		id["type"] = "DatabaseFrontend";
		id["username"] = mUsername;

		LLSD auth = LLSD::emptyMap();
		auth["type"] = "DatabaseFrontend";
		auth["creds"] = mPassword;

		LLPointer<LLCredential> databaseCred = gSecAPIHandler->createCredential("DatabaseFrontend", id, auth);
		gSecAPIHandler->saveCredential(databaseCred, true);
	}
	
	password = mPassword + exoSystems::databaseBackendSalt;

	LLMD5 md5((const U8 *)password.c_str());
	char hash[33]; /* Flawfinder: ignore */
	md5.hex_digest(hash);

	LLSD headers; // HTTP_*
	headers.insert("AGENT_VERSION", LLVersionInfo::getAgentVersion());
	headers.insert("AGENT_KEY", gAgent.getID());
	headers.insert("TARGET_KEY", mTargetKey.asString());
	headers.insert("REQUEST_TYPE", "INVITE");
	headers.insert("DATABASE_USERNAME", username);
	headers.insert("DATABASE_PASSWORD", hash);
	headers.insert("REQUEST_NAME", target);
	headers.insert("REQUEST_RANK", getChild<LLComboBox>("Rank")->getCurrentIndex());
	headers.insert("REQUEST_BALANCE", getChild<LLSpinCtrl>("Balance")->getValue().asInteger());
	headers.insert("REQUEST_DIVISION", getChild<LLComboBox>("Division")->getSelectedValue());

	LLHTTPClient::get(exoSystems::databaseBackendLocation, new exoDatabaseClient(mTargetKey, "ark_database_frontend_invite"), headers);
}

void exoFloaterDatabaseFrontendInvite::onClickInvite()
{
	uuid_vec_t agentKeys;
	agentKeys.push_back(mTargetKey);

	LLFloaterGroupInvite::showForGroup(getChild<LLComboBox>("Group")->getCurrentID(), &agentKeys);
}

void exoFloaterDatabaseFrontendInvite::recieveData(LLSD data, bool status)
{
	if (status && data.has("valid"))
	{
		setCanClose(TRUE);
		closeFloater();
	}
	else
	{
		LLNotificationsUtil::add("ExodusFrontendError", LLSD());
		
		setCanClose(TRUE);
		closeFloater();
	}
}
