/** 
 * @file postDeferredNoDoFF.glsl
 *
 * $LicenseInfo:firstyear=2007&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2007, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */
 
#extension GL_ARB_texture_rectangle : enable

#ifdef DEFINE_GL_FRAGCOLOR
out vec4 frag_color;
#else
#define frag_color gl_FragColor
#endif

uniform sampler2DRect diffuseRect;
uniform sampler2D bloomMap;

uniform vec2 screen_res;
VARYING vec2 vary_fragcoord;

//<exodus>
uniform float exo_vignette;

float Luminance(vec3 color)
{
	return dot(color, vec3(0.22, 0.707, 0.071));
}
//</exodus>

void main() 
{
	vec4 diff = texture2DRect(diffuseRect, vary_fragcoord.xy) + texture2D(bloomMap, vary_fragcoord.xy/screen_res);
	
	if (exo_vignette > 0)
	{
		vec2 tc = vary_fragcoord.xy/screen_res - 0.5f;
		float vignette = 1 - dot(tc, tc);
		diff.rgb *= mix(1, vignette * vignette * vignette * vignette, exo_vignette);
	}
	frag_color = diff;
}

